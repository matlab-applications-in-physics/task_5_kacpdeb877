%MATLAB R2020b
%name:resonance
%author:kacpdeb877
%date: 7.12.2020
%version: v1.0

clc; clear;

r_zwoju = (1/12)*0.0254; %promień zwoju wyrażony w m
m = 0.1 %masa wyrażona w kg
t = 10 %stała czsowa wyrażona w s
n = 40 %liczba zwojów
g_ziem = 9.81 %przyspieszenie ziemskie wyrażone w m/s^2
g_modul =  %modył sztywności materiału
b = 